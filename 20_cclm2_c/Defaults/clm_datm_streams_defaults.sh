#!/bin/bash

place_holder(){
  echo "This is a place holder function, please implement ${1} sensible defaults or in a case specific config."
  exit 1
}

# ---------------------------------------------------------------
#                            datm.stream.Precip
# ---------------------------------------------------------------
clm_c_datm_stream_Precip(){
  place_holder ${FUNCNAME[0]}
}
export -f clm_c_datm_stream_Precip

# ---------------------------------------------------------------
#                            datm.stream.Solar
# ---------------------------------------------------------------
clm_c_datm_stream_Solar(){
  place_holder ${FUNCNAME[0]}
}
export -f clm_c_datm_stream_Solar

# ---------------------------------------------------------------
#                            datm.stream.TPQW
# ---------------------------------------------------------------
clm_c_datm_stream_TPQW(){
  place_holder ${FUNCNAME[0]}
}
export -f clm_c_datm_stream_TPQW

# ---------------------------------------------------------------
#                            datm.stream.presaero
# ---------------------------------------------------------------
clm_c_datm_stream_presaero(){
  place_holder ${FUNCNAME[0]}
}
export -f clm_c_datm_stream_presaero

# ---------------------------------------------------------------
#                            datm.stream.topo
# ---------------------------------------------------------------
clm_c_datm_stream_topo(){
  place_holder ${FUNCNAME[0]}
}
export -f clm_c_datm_stream_topo
